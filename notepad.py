#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tkinter import *
from tkinter import filedialog

class Demo1:
    def __init__(self, master):
        self.master = master
        self.frame = Frame(self.master)
        self.master.geometry("700x500")
        self.master.title("Notepad")
        self.textbox(self.master)
        self.addmenu(self.master)
        self.frame.pack()

    def newFile(self):
        self.T.delete('1.0', END)

    def openFile(self):
        self.T.delete('1.0', END)

        fileName = filedialog.askopenfilename(filetypes=[("Text Files", "*.txt")])
        print(fileName)

        fobj = open(fileName, "r")
        data = fobj.readlines()
        fobj.close()

        self.T.insert(END, data)

    def saveFile(self):
        fileName = filedialog.asksaveasfilename(filetypes=[("Text Files", "*.txt")])
        data = self.T.get(1.0, "end-1c")
        print(fileName)

        fobj = open(fileName, "w")
        fobj.write(data)
        fobj.close()

    def addmenu(self, root):
        menubar = Menu(root, bd=3)
        fileMenu = Menu(menubar, tearoff = 0)
        fileMenu.add_command(label="New", command=self.newFile, font=("Helvetica", 10, "bold"), background="coral", foreground="azure")
        fileMenu.add_command(label ="Open", command=self.openFile, font=("Helvetica", 10, "bold"), background="coral", foreground="azure")
        fileMenu.add_command(label ="Save", command=self.saveFile, font=("Helvetica", 10, "bold"), background="coral", foreground="azure")
        fileMenu.add_separator()
        fileMenu.add_command(label ="Exit", command = root.quit, font=("Helvetica", 10, "bold"), background="coral", foreground="azure")
        menubar.add_cascade(label ="F10⬇", menu = fileMenu, font=("Helvetica", 12, "bold"), background="coral", foreground="azure")

        root.config(menu = menubar)

    def textbox(self, root):
        self.T = Text(root, height=20, width=60, background="cadetblue", foreground="azure", font=("Helvetica", 20, "bold"), bd=5, relief="groove")
        self.S = Scrollbar(root, background="coral", bd=3, relief="groove")
        self.S.pack(side=RIGHT, fill=Y)
        self.T.pack(side=LEFT, fill=Y)
        self.S.config(command=self.T.yview)
        self.T.config(yscrollcommand=self.S.set)
        self.T.pack()


def main():
    root = Tk()
    app = Demo1(root)
    frame = Frame(root, bd=5, relief="groove")
    root.mainloop()


if __name__ == '__main__':
    main()
